#!/bin/bash

echo "list(APPEND SOURCES " > include/CMakeLists.txt
for src in include/*; do
    if [[ "$src" =~ \.(h|hpp)$ ]]; then
        echo '    ${CMAKE_CURRENT_LIST_DIR}'/$(basename $src) >> include/CMakeLists.txt
    fi
done
echo ")" >> include/CMakeLists.txt


echo "list(APPEND SOURCES " > source/CMakeLists.txt
for src in source/*; do
    if [[ "$src" =~ \.(c|cpp|cxx)$ ]]; then
        echo '    ${CMAKE_CURRENT_LIST_DIR}'/$(basename $src) >> source/CMakeLists.txt
    fi
done
echo ")" >> source/CMakeLists.txt



cmake -B build
cd build
make
cd ..

cmake -B build_win32 -DCMAKE_TOOLCHAIN_FILE=/usr/share/mingw/toolchain-x86_64-w64-mingw32.cmake
cd build_win32
make
cd ..
