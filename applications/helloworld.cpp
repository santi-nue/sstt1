#include <iostream>

#include "util.h"


#ifdef _WIN32
#include <windows.h>
 
int WINAPI WinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR lpCmdLine,
    int nCmdShow
)
{
	UNUSED(hInstance);
	UNUSED(hPrevInstance);
	UNUSED(lpCmdLine);
	UNUSED(nCmdShow);

    std::cout << "Hello World." << std::endl;
    MessageBox(NULL, "Hello World.", "Hello", MB_OK);
    return 0;
}

#else

int main(int argc, char* argv[])
{
	UNUSED(argc);
	UNUSED(argv);
    std::cout << "Hello World." << std::endl;
}
#endif
